<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function Register(){
        return view('halaman.Register');
    }
    public function Welcome(Request $request){
       $first_name = $request['nama depan'];
       $last_name = $request['nama belakang'];
       return view('halaman.Welcome', Compact('nama depan','nama belakang'));
    }
}
